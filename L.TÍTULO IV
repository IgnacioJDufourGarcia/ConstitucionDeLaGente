TÍTULO IV

Del Gobierno y de la Administración

Artículo 97.
El Gobierno dirige la política interior y exterior, la Administración civil y militar y la
defensa del Estado. Ejerce la función ejecutiva y la potestad reglamentaria de acuerdo con la
Constitución y las leyes.

Artículo 98.
1. El Gobierno se compone del Presidente, de los Vicepresidentes, en su caso, de los
Ministros y de los demás miembros que establezca la ley.
2. El Presidente dirige la acción del Gobierno y coordina las funciones de los demás
miembros del mismo, sin perjuicio de la competencia y responsabilidad directa de éstos en
su gestión.
3. Los miembros del Gobierno no podrán ejercer otras funciones representativas que las
propias del mandato parlamentario, ni cualquier otra función pública que no derive de su
cargo, ni actividad profesional o mercantil alguna.
4. La ley regulará el estatuto e incompatibilidades de los miembros del Gobierno.

Artículo 99.
1. Después de cada renovación del Congreso de los Diputados, y en los demás
supuestos constitucionales en que así proceda, el Rey, previa consulta con los
representantes designados por los Grupos políticos con representación parlamentaria, y a
través del Presidente del Congreso, propondrá un candidato a la Presidencia del Gobierno.
2. El candidato propuesto conforme a lo previsto en el apartado anterior expondrá ante el
Congreso de los Diputados el programa político del Gobierno que pretenda formar y
solicitará la confianza de la Cámara.
3. Si el Congreso de los Diputados, por el voto de la mayoría absoluta de sus miembros,
otorgare su confianza a dicho candidato, el Rey le nombrará Presidente. De no alcanzarse
dicha mayoría, se someterá la misma propuesta a nueva votación cuarenta y ocho horas
después de la anterior, y la confianza se entenderá otorgada si obtuviere la mayoría simple.
4. Si efectuadas las citadas votaciones no se otorgase la confianza para la investidura,
se tramitarán sucesivas propuestas en la forma prevista en los apartados anteriores.
5. Si transcurrido el plazo de dos meses, a partir de la primera votación de investidura,
ningún candidato hubiere obtenido la confianza del Congreso, el Rey disolverá ambas
Cámaras y convocará nuevas elecciones con el refrendo del Presidente del Congreso.

Artículo 100.
Los demás miembros del Gobierno serán nombrados y separados por el Rey, a
propuesta de su Presidente.

Artículo 101.
1. El Gobierno cesa tras la celebración de elecciones generales, en los casos de pérdida
de la confianza parlamentaria previstos en la Constitución, o por dimisión o fallecimiento de
su Presidente.
2. El Gobierno cesante continuará en funciones hasta la toma de posesión del nuevo
Gobierno.

Artículo 102.
1. La responsabilidad criminal del Presidente y los demás miembros del Gobierno será
exigible, en su caso, ante la Sala de lo Penal del Tribunal Supremo.
2. Si la acusación fuere por traición o por cualquier delito contra la seguridad del Estado
en el ejercicio de sus funciones, sólo podrá ser planteada por iniciativa de la cuarta parte de
los miembros del Congreso, y con la aprobación de la mayoría absoluta del mismo.
3. La prerrogativa real de gracia no será aplicable a ninguno de los supuestos del
presente artículo.

Artículo 103.
1. La Administración Pública sirve con objetividad los intereses generales y actúa de
acuerdo con los principios de eficacia, jerarquía, descentralización, desconcentración y
coordinación, con sometimiento pleno a la ley y al Derecho.
2. Los órganos de la Administración del Estado son creados, regidos y coordinados de
acuerdo con la ley.
3. La ley regulará el estatuto de los funcionarios públicos, el acceso a la función pública
de acuerdo con los principios de mérito y capacidad, las peculiaridades del ejercicio de su
derecho a sindicación, el sistema de incompatibilidades y las garantías para la imparcialidad
en el ejercicio de sus funciones.

Artículo 104.
1. Las Fuerzas y Cuerpos de seguridad, bajo la dependencia del Gobierno, tendrán
como misión proteger el libre ejercicio de los derechos y libertades y garantizar la seguridad
ciudadana.
2. Una ley orgánica determinará las funciones, principios básicos de actuación y
estatutos de las Fuerzas y Cuerpos de seguridad.

Artículo 105.
La ley regulará:
a) La audiencia de los ciudadanos, directamente o a través de las organizaciones y
asociaciones reconocidas por la ley, en el procedimiento de elaboración de las disposiciones
administrativas que les afecten.
b) El acceso de los ciudadanos a los archivos y registros administrativos, salvo en lo que
afecte a la seguridad y defensa del Estado, la averiguación de los delitos y la intimidad de las
personas.
c) El procedimiento a través del cual deben producirse los actos administrativos,
garantizando, cuando proceda, la audiencia del interesado.

Artículo 106.
1. Los Tribunales controlan la potestad reglamentaria y la legalidad de la actuación
administrativa, así como el sometimiento de ésta a los fines que la justifican.
2. Los particulares, en los términos establecidos por la ley, tendrán derecho a ser
indemnizados por toda lesión que sufran en cualquiera de sus bienes y derechos, salvo en
los casos de fuerza mayor, siempre que la lesión sea consecuencia del funcionamiento de
los servicios públicos.

Artículo 107.
El Consejo de Estado es el supremo órgano consultivo del Gobierno. Una ley orgánica
regulará su composición y competencia.